/**
 * 
 */
/**
 * @author SVTI
 *
 */
open module helloworld {

	requires java.sql;
	requires java.instrument;
	requires java.xml.bind;

	requires spring.beans;
	requires spring.boot;
	requires spring.boot.autoconfigure;
	requires spring.context;
	requires spring.core;
	requires spring.web;
	requires spring.webmvc;
}